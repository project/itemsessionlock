<?php

/**
 * @file
 * Install hooks for item session lock module.
 * 
 */

/**
 * Implements hook_schema().
 */
function itemsessionlock_schema() {
  $schema = array();
  $schema['itemsessionlock'] = array(
    'description' => "Stores a map of user session id and arbitrary items uuids.",
    'fields' => array(
      'uid' => array(
        'description' => 'The {users}.uid corresponding to a session, or 0 for anonymous user.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'sid' => array(
        'description' => "A session ID.",
        'type' => 'varchar',
        'length' => 128,
        'not null' => TRUE,
      ),
      'ssid' => array(
        'description' => "Secure session ID.",
        'type' => 'varchar',
        'length' => 128,
        'not null' => FALSE,
        'default' => '',
      ),
      'iid' => array(
        'description' => "Unique identifier of the item to lock",
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'module' => array(
        'description' => 'The name of the module setting this lock.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'type' => array(
        'description' => 'The type of item the lock is on.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'timestamp' => array(
        'description' => 'The Unix timestamp when this lock was made.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'data' => array(
        'description' => 'Generic data storage.',
        'type' => 'blob',
        'not null' => FALSE,
        'size' => 'big',
      ),
    ),
    'primary key' => array(
      'iid',
      'module',
    ),
    'indexes' => array(
      'timestamp' => array('timestamp'),
      'uid' => array('uid'),
      'sid' => array('ssid'),
    ),
    'foreign keys' => array(
      'itemsessionlock_user' => array(
        'table' => 'users',
        'columns' => array('uid' => 'uid'),
      ),
      'itemsessionlock_session' => array(
        'table' => 'sessions',
        'columns' => array('sid' => 'sid'),
      ),
    ),
  );
  return $schema;
}
